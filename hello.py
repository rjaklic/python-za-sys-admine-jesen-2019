from flask import Flask, request, jsonify
app = Flask(__name__)

@app.route('/hello')
def hello_world():
    username = request.args.get("username")
    password = request.args.get("password")

    print(username)

    return jsonify({'token': '123'})

@app.route('/world')
def world():

    return jsonify({'lala': 'lelele'})
