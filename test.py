import imaplib
import smtplib
import ssl
import email
import random
import string
import time
import os

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from config import arnes, gmail, headless


class WebmailArnesSi(unittest.TestCase):
    tmp_dir = os.getcwd() + '/tmp'
    tmp_file = os.getcwd() + '/tmp.txt'
    geckodriver = os.getcwd() + '/geckodriver'
    urls = {
        'root': 'https://webmail.arnes.si',
        'compose': 'https://webmail.arnes.si/?_task=mail&_action=compose',
        'white_list': 'https://webmail.arnes.si/?_task=settings&_action=plugin.spamoz&_list=white',
        'black_list': 'https://webmail.arnes.si/?_task=settings&_action=plugin.spamoz&_list=black',
        'change_password': 'https://webmail.arnes.si/?_task=settings&_action=plugin.password',
        'vacation': 'https://webmail.arnes.si/?_task=settings&_action=plugin.vacation',
        'forward': 'https://webmail.arnes.si/?_task=settings&_action=plugin.forward'
    }
    random_subject = 'naslov maila '
    random_email_body = 'vsebina '
    random_chars = ''
    timeout = 4
    arnes = arnes
    gmail = gmail
    headless = headless

    @classmethod
    def setUpClass(cls):
        if not os.path.exists(cls.tmp_dir):
            os.mkdir(cls.tmp_dir)

        # if os.path.exists(cls.tmp_file):
        #    os.unlink(cls.tmp_file)

        options = Options()
        options.headless = cls.headless
        cls.random_chars = ''.join(random.choice(string.ascii_letters + string.digits) for x in range(100))

        cls.random_subject = cls.random_subject + cls.random_chars
        cls.random_email_body = cls.random_email_body + cls.random_chars

        cls.driver = webdriver.Firefox(executable_path=cls.geckodriver, options=options)

    # @unittest.skip
    def test001_login(self):
        driver = self.driver
        driver.get(self.urls['root'])
        try:
            element_present = EC.presence_of_element_located((By.ID, 'rcmloginuser'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert "Could not get to compose message page"

        if not "Dobrodošli v webmail.arnes.si" in driver.title:
            print(driver.title)
            assert False, "Not proper title on index page"

        elem = driver.find_element_by_id("rcmloginuser")
        elem.send_keys(self.arnes['username'])

        elem = driver.find_element_by_id("rcmloginpwd")
        elem.send_keys(self.arnes['password'])
        elem.send_keys(Keys.RETURN)
        try:
            element_present = EC.presence_of_element_located((By.ID, 'logo'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Could not login"

        time.sleep(self.timeout)

    # @unittest.skip
    def test002_send_email_with_attachment_from_webmail(self):
        driver = self.driver
        driver.get(self.urls['compose'])
        try:
            element_present = EC.presence_of_element_located((By.ID, '_to'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert "Could not get to compose message page"

        if not "Sestavi sporočilo" in driver.title:
            assert False, "Not proper title on compose page"

        elem = driver.find_element_by_id('_to')
        elem.send_keys('arnestest2@gmail.com')

        elem = driver.find_element_by_id('compose-subject')
        elem.send_keys(self.random_subject)

        try:
            element_present = EC.presence_of_element_located((By.ID, 'compose-body_ifr'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Compose body iframe not loaded"

        time.sleep(10)

        driver.switch_to.frame(driver.find_element_by_id("compose-body_ifr"))

        elem = driver.find_element_by_id('tinymce')
        elem.send_keys(self.random_email_body)

        driver.switch_to.default_content()
        elem = driver.find_element_by_id('uploadmenulink')
        elem.click()

        webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()

        elem = driver.find_element_by_id('attachment-formInput')
        elem.send_keys(self.tmp_file)

        elem = driver.find_element_by_css_selector("#attachment-formFrm input.button.mainaction")
        elem.click()

        # todo
        time.sleep(20)

    @unittest.skip
    def test003_check_email_on_gmail_sent_from_webmail(self):
        gmail = imaplib.IMAP4_SSL(self.gmail['imap'])
        gmail.login(self.gmail['username'], self.gmail['password'])
        gmail.select('inbox')

        result, data = gmail.search(None, '(SUBJECT "%s")' % (self.random_chars, ))
        if result != 'OK':
            assert False, "Mail was not delivered or something went wrong."

        if len(data[0].split()) < 1:
            assert False, "Mail was not delivered or something went wrong."

        for num in data[0].split():
            result, data = gmail.uid('fetch', num, '(RFC822)')
            if result == 'OK':
                email_message = email.message_from_bytes(data[0][1])
                if email_message.is_multipart():
                    for part in email_message.walk():
                        if part.get_content_type() == "text/plain":
                            body = part.get_payload(decode=True)
                            body = body.decode()

                            tmp_file_data = open(self.tmp_file, 'r').read()
                            if tmp_file_data not in body:
                                assert False, "Mail was not delivered or something went wrong."
                            tmp_file_data.close()

                        filename = part.get_filename()
                        if filename is not None:
                            sv_path = os.path.join(self.tmp_dir, filename)
                            if not os.path.isfile(sv_path):
                                fp = open(sv_path, 'wb')
                                fp.write(part.get_payload(decode=True))
                                fp.close()

        if not os.path.exists(self.tmp_dir + "/geckodriver"):
            assert False, "No attachment arrived?"

        time.sleep(self.timeout)

    @unittest.skip
    def test004_send_email_from_gmail_to_arnes(self):
        msg = MIMEMultipart()
        msg['From'] = self.gmail['username']
        msg['To'] = self.arnes['email']
        msg['Subject'] = 'naslov maila %s' % self.random_chars
        msg.attach(MIMEText('vsebina %s' % self.random_chars))

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(self.gmail['smtp'], self.gmail['smtp_port'], context=context) as server:
            server.login(self.gmail['username'], self.gmail['password'])
            server.sendmail(self.gmail['username'], self.arnes['email'], msg.as_string())

        time.sleep(self.timeout)

    @unittest.skip
    def test005_check_email_sent_from_gmail_to_arnes(self):
        driver = self.driver
        driver.get(self.urls['root'])
        try:
            element_present = EC.presence_of_element_located((By.ID, 'quicksearchbox'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert "Could not get to root message page"

        time.sleep(self.timeout)

        # check email
        elem = driver.find_element_by_id('quicksearchbox')
        elem.send_keys("%s" % (self.random_chars, ))
        elem.send_keys(Keys.ENTER)

        time.sleep(self.timeout)

        try:
            element_present = EC.presence_of_element_located((By.CSS_SELECTOR, '#messagelist tbody tr'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Message list table was not found."

        elem = driver.find_element_by_css_selector("#messagelist tbody tr")
        elem.click()

        driver.switch_to.frame(driver.find_element_by_id("messagecontframe"))

        elem = driver.find_element_by_id("messagebody")
        if not self.random_chars in elem.text:
            assert False, "Message not found."

        driver.switch_to.default_content()
        time.sleep(self.timeout)

    @unittest.skip
    def test006_add_to_white_list(self):
        driver = self.driver
        driver.get(self.urls['white_list'])

        driver.switch_to.frame(driver.find_element_by_id("cc_ar_manager_frame"))

        try:
            element_present = EC.presence_of_element_located((By.ID, 'rcso_email'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Could not get white list page"

        time.sleep(self.timeout)

        elem = driver.find_element_by_id('rcso_email')
        elem.send_keys('zivali.na@internet.com')

        elem = driver.find_element_by_id('rcmbtn102')
        elem.click()

        alert = driver.switch_to.alert
        alert.accept()

        time.sleep(self.timeout)

        elem = driver.find_element_by_css_selector("#spamlist-table tbody tr")
        if not elem:
            assert False, "Could not add email to white list"

        elem = driver.find_element_by_css_selector("#spamlist-table tbody tr#rcmrow1 .delete a")
        if not elem:
            assert False, "Could not remove email to white list"
        elem.click()
        alert = driver.switch_to.alert
        alert.accept()

        time.sleep(self.timeout)

        # not working right now
        # elem = driver.find_element_by_css_selector("#spamlist-table tbody tr")
        # if elem:
        #     assert False, "Spam list should be empty"

        driver.switch_to.default_content()
        time.sleep(self.timeout)

    @unittest.skip
    def test007_add_to_black_list(self):
        driver = self.driver
        driver.get(self.urls['black_list'])

        driver.switch_to.frame(driver.find_element_by_id("cc_ar_manager_frame"))

        try:
            element_present = EC.presence_of_element_located((By.ID, 'rcso_email'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Could not get white list page"

        time.sleep(self.timeout)

        elem = driver.find_element_by_id('rcso_email')
        elem.send_keys('zivali.na@internet.com')

        elem = driver.find_element_by_id('rcmbtn102')
        elem.click()

        alert = driver.switch_to.alert
        alert.accept()

        time.sleep(self.timeout)

        elem = driver.find_element_by_css_selector("#spamlist-table tbody tr")
        if not elem:
            assert False, "Could not add email to black list"

        elem = driver.find_element_by_css_selector("#spamlist-table tbody tr#rcmrow1 .delete a")
        if not elem:
            assert False, "Could not remove email to black list"
        elem.click()
        alert = driver.switch_to.alert
        alert.accept()

        time.sleep(self.timeout)

        # not working right now
        # elem = driver.find_element_by_css_selector("#spamlist-table tbody tr:visible")
        # if elem:
        #     assert False, "Spam list should be empty"

        driver.switch_to.default_content()
        time.sleep(self.timeout)

    @unittest.skip
    def test008_change_password(self):
        driver = self.driver
        driver.get(self.urls['change_password'])

        try:
            element_present = EC.presence_of_element_located((By.ID, 'curpasswd'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Could not get to change passoword plugin page"

        elem = driver.find_element_by_id('curpasswd')
        elem.send_keys('123')

        elem = driver.find_element_by_id('newpasswd')
        elem.send_keys('Test12345')

        elem = driver.find_element_by_id('confpasswd')
        elem.send_keys('Test12345')

        elem = driver.find_element_by_id('rcmbtn112')
        elem.click()

        driver.switch_to.default_content()

        elem = driver.find_element_by_css_selector('.button-logout')
        elem.click()

        time.sleep(self.timeout)

        self.arnes['password'] = 'Test12345'
        self.test001_login()

    @unittest.skip
    def test009_vacation(self):
        driver = self.driver
        driver.get(self.urls['vacation'])

        time.sleep(self.timeout)

        driver.switch_to.frame(driver.find_element_by_id("vacation_frame"))

        try:
            element_present = EC.presence_of_element_located((By.ID, 'vacation_enabled'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Could not get to vacation plugin page"

        elem = driver.find_element_by_id('vacation_enabled')
        elem.click()

        elem = driver.find_element_by_id('vacation_subject')
        elem.clear()
        elem.send_keys('Samodejni odgovor test %s' % self.random_chars)

        elem = driver.find_element_by_id('vacation_body')
        elem.clear()
        elem.send_keys('%s' % self.random_chars)

        elem = driver.find_element_by_id('rcmbtn102')
        elem.click()

        msg = MIMEMultipart()
        msg['From'] = self.gmail['username']
        msg['To'] = self.arnes['email']
        msg['Subject'] = 'Vacation test'

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(self.gmail['smtp'], self.gmail['smtp_port'], context=context) as server:
            server.login(self.gmail['username'], self.gmail['password'])
            server.sendmail(self.gmail['username'], self.arnes['email'], msg.as_string())

        time.sleep(self.timeout)

        # check email
        # gmail = imaplib.IMAP4_SSL(self.gmail['imap'])
        # gmail.login(self.gmail['username'], self.gmail['password'])
        # gmail.select('inbox')
        #
        # result, data = gmail.search(None, '(SUBJECT "Samodejni odgovor test %s")' % self.random_chars)
        # if result != 'OK':
        #     assert False, "Mail was not delivered or something went wrong."
        #
        # if len(data[0].split()) < 1:
        #     assert False, "Mail was not delivered or something went wrong."

        elem = driver.find_element_by_id('vacation_enabled')
        elem.click()

        time.sleep(self.timeout)

    @unittest.skip
    def test010_forward(self):
        driver = self.driver
        driver.get(self.urls['forward'])

        time.sleep(self.timeout)

        try:
            element_present = EC.presence_of_element_located((By.ID, '_new_forward'))
            WebDriverWait(driver, self.timeout).until(element_present)
        except TimeoutException:
            assert False, "Could not get to vacation plugin page"

        elem = driver.find_element_by_id('_new_forward')
        elem.send_keys(self.arnes['forward'])

        elem = driver.find_element_by_id('rcmbtn112')
        elem.click()

        time.sleep(self.timeout)

        driver.get('https://webmail.arnes.si/?_task=settings&_action=plugin.forward-delete&mail=bill.gate%40internet.com')

        time.sleep(self.timeout)


    @classmethod
    def tearDownClass(self):
        # self.driver.close()
        pass


if __name__ == "__main__":
    unittest.main()
