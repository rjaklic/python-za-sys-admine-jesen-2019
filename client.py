# client.py
import socket

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 10000)
print('connecting to %s port %s' % server_address)
sock.connect(server_address)

try:
    # Send data
    message = b'zivjo'
    print('sending "%s"' % message)
    sock.sendall(message)

    # Look for the response
    data = sock.recv(1024)
    print('received "%s"' % data)

finally:
    print('closing socket')
    sock.close()
