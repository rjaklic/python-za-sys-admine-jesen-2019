zaloga = []

class Avto:
    znamka = ""
    model = ""
    letnik = ""
    stevilo_vrat = ""
    seriska = ""

    def set_znamka(self, znamka):
        self.znamka = znamka

    @staticmethod
    def isci(zaloga, iskani_string):
        najdeni_elementi = []

        for z in zaloga:
            if iskani_string in z.znamka:
                najdeni_elementi.append(z)
            elif iskani_string in z.model:
                najdeni_elementi.append(z)
            elif iskani_string in z.letnik:
                najdeni_elementi.append(z)
            elif iskani_string in z.stevilo_vrat:
                najdeni_elementi.append(z)
            elif iskani_string in z.seriska:
                najdeni_elementi.append(z)

        return najdeni_elementi

    # def __str__(self):
    #     return "sdfsdfsdf " + self.znamka

    def izpisi(self):
        return self.znamka + ":" + self.model

class Zavarovanje:
    pass


if __name__ == "__main__":
    a1 = Avto()
    a1.znamka = "bmw"
    a1.model = "s3"
    a1.letnik = "2000"
    a1.stevilo_vrat = "4"
    a1.seriska = "1234"

    a2 = Avto()
    a2.znamka = "skoda"
    a2.model = "octavia"
    a2.letnik = "2001"
    a2.stevilo_vrat = "4"
    a2.seriska = "1235"

    zaloga.append(a1)
    zaloga.append(a2)

    najdeni = Avto.isci(zaloga, "1235")
    prvi_element = najdeni[0]

    print(prvi_element.izpisi())
    # print(Zavarovanje.isci(zaloga, "1235"))

