# server.py
import socket

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 10000)
print('starting up on %s port %s' % server_address)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    print("tukaj sem 1")

    try:
        print('connection from %s:%s' % (client_address))

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(1024)
            print('received %s' % data)
            if data:
                print('sending data back to the client')
                if data.decode("utf-8") == "zivjo":
                    connection.sendall(b'zivjo, kako si kaj?')
                else:
                    connection.sendall(b'zakaj ne pozdravis?')
            else:
                print('no more data from %s:%s' % client_address)
                break
    except Exception as e:
        print("tukaj sem 2")
    finally:
        print("tukaj sem 3")
        # Clean up the connection
        connection.close()
