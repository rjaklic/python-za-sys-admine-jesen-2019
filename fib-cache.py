cache = {}


def fib(n):
    global cache

    print("------")
    print("in function fib, n == %s" % (n,))
    print("------")

    if n in cache:
        return cache[n]

    if n == 1 or n == 2:
        return 1
    else:
        result = fib(n-1) + fib(n-2)
        cache[n] = result

        return result


print("rezultat: ", end="")
print(fib(100))
