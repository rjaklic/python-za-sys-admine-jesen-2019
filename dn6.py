sez=[0,1]
    
while len(sez) < 10000:
    sez.append(sez[-1]+sez[-2])

print("1: ", end="")
print(len(sez))

sez2 = []
for x in sez:
    # x = 10
    # x%2
    # 10%2 = 0
    # 10/2 = 5 ost. 0

    if x%2==0:
        sez2.append(x)

print("2: ", end="")
print(len(sez2))

sez3 = []
for x in sez2:
    # 1. x = 2584 to je zdej tipa int
    # 2. x = "2584 "to je zdej tipa int
    soda = 0
    for n in str(x):
        if n == "0" or int(n)%2==0:
            soda += 1
    sez3.append(soda)

print("3: ", end="")
print(sez2[:100])
print(sez3[:100])
print(len(sez3))

print("4: ", end="")
vsota = 0
for x in sez3:
    vsota += x
print(vsota)
print(sum(sez3))

sez5 = {}
for x in sez:
    vseh = 0
    for y in str(x):
        vseh += int(y)
    sez5[x] = vseh

# print(sez5)

print("6: ", end="")
key=sez[7777]
print(sez5[key])
print("kljuc: " + str(key))
